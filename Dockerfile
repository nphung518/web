# Sử dụng hình ảnh Node.js v14
FROM node:14-alpine

# Thiết lập thư mục làm việc trong container
WORKDIR /app

# Sao chép package.json và package-lock.json vào thư mục làm việc
COPY package*.json ./

# Cài đặt các phụ thuộc
RUN npm install

# Sao chép toàn bộ mã nguồn ứng dụng vào thư mục làm việc
COPY . .

# Khai báo cổng mà ứng dụng sẽ lắng nghe
EXPOSE 4200

# Khởi chạy lệnh để khởi động ứng dụng
CMD ["npm", "run", "start"]
